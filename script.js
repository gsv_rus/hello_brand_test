const swiper = new Swiper('.swiper', {
    // Optional parameters
    direction: 'horizontal',
    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    allowTouchMove: true,
    autoplay: true,
    loop: true,

    breakpoints: {

        950: {
            allowTouchMove: false,
            autoplay: false,
            loop: false,
        }
    }
});

$(document).ready(function () {
    $("#form").submit(function (e) { //устанавливаем событие отправки для формы с id=form
        e.preventDefault()
        let form_data = $(this).serialize(); //собераем все данные из формы
        $.ajax({
            type: 'POST', //Метод отправки
            url: 'send.php', //путь до php фаила отправителя
            data: form_data,
            success: function (data) { // сoбытиe пoслe удaчнoгo oбрaщeния к сeрвeру и пoлучeния oтвeтa
                alert('все ок'); // пoкaжeм eё тeкст
            }
        });
    });
});

const modal = document.querySelector(".modal");
const trigger = document.querySelector(".trigger");
const closeButton = document.querySelector(".close-button");

function toggleModal() {
    modal.classList.toggle("show-modal");
}

function windowOnClick(event) {
    if (event.target === modal) {
        toggleModal();
    }
}

trigger.addEventListener("click", toggleModal);
closeButton.addEventListener("click", toggleModal);
window.addEventListener("click", windowOnClick);


